// Fill out your copyright notice in the Description page of Project Settings.


#include "DeathMalus.h"
#include "SnakeBase.h"
#include "PlayerPawnBase.h"

// Sets default values
ADeathMalus::ADeathMalus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ADeathMalus::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ADeathMalus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	BufferTimeDeath += DeltaTime;
	if (BufferTimeDeath > StepDelayDeath)
	{
		Destroy(true, true);

	}
}

void ADeathMalus::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		auto PlayerPawn = Cast<APlayerPawnBase>(Snake->GetOwner());

		if (PlayerPawn)
		{
			PlayerPawn->LifeDecrease();
			Snake->DestroySnake();
			Snake->Destroy(true, true);
		}
	}
}






