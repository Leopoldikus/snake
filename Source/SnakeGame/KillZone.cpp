// Fill out your copyright notice in the Description page of Project Settings.


#include "KillZone.h"
#include "SnakeBase.h"

#include "SnakeElementBase.h"

// Sets default values
AKillZone::AKillZone()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AKillZone::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AKillZone::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	

}

void AKillZone::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		
		auto PlayerPawn = Cast<APlayerPawnBase>(Snake->GetOwner());
		
		if (PlayerPawn)
		{
			PlayerPawn->LifeDecrease();
			Snake->DestroySnake();
			Snake->Destroy(true, false);

		}
		
	}
}



