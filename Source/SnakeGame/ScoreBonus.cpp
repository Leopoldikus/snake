// Fill out your copyright notice in the Description page of Project Settings.


#include "ScoreBonus.h"
#include "SnakeBase.h"
#include "Food.h"
#include "PlayerPawnBase.h"

// Sets default values
AScoreBonus::AScoreBonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AScoreBonus::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AScoreBonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	BufferTimeSpeed += DeltaTime;
	if (BufferTimeSpeed > StepDelaySpeed)
	{
		Destroy(true, true);

	}

}

void AScoreBonus::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			//ScoreUpdate.Broadcast();
			auto PlayerPawn = Cast<APlayerPawnBase>(Snake->GetOwner());
			PlayerPawn->BonusScoreUP();
			Destroy();
		}
	}
}


