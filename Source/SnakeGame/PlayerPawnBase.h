// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Food.h"
#include "Bonus.h"
#include "DeathMalus.h"
#include "ScoreBonus.h"
#include "PlayerPawnBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FGameOver);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FWin);

class UCameraComponent;
class ASnakeBase;
class AFood;
class AKillZone;
class ABonus;
class ADeathMalus;
class AScoreBonus;


UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
	ASnakeBase* SnakeActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeBase> SnakeActorClass;

	UPROPERTY(BlueprintReadWrite)
	AFood* FoodActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodActorClass;

	UPROPERTY(BlueprintReadWrite)
	AKillZone* KillZoneActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AKillZone> KillZoneActorClass;

	UPROPERTY(BlueprintReadWrite)
	ABonus* BonusActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABonus> BonusActorClass;

	UPROPERTY(BlueprintReadWrite)
	ADeathMalus* DeathActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ADeathMalus> DeathActorClass;

	UPROPERTY(BlueprintReadWrite)
	AScoreBonus* ScoreBonusActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AScoreBonus> ScoreBonusActorClass;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 Life = 1;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 Score = 0;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 WinScore = 50;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	UFUNCTION(BlueprintCallable, Category="SnakePawn")
	void CreateSnakeActor();

	UFUNCTION()
	void HandlePlayerVerticalInput(float value);

	UFUNCTION()
	void HandlePlayerHorizontalInput(float value);

	UFUNCTION(BlueprintCallable)
	void AddRandomFood();

	UFUNCTION(BlueprintCallable)
	void InstantKill();

	UFUNCTION(BlueprintCallable)
	void SpawnSpeedBonus();

	UFUNCTION(BlueprintCallable)
	void SpawnScoreBonus();

	UFUNCTION(BlueprintCallable)
	void ScoreUP();

	UFUNCTION(BlueprintCallable)
	void BonusScoreUP();

	UFUNCTION(BlueprintCallable)
	void LifeDecrease();
		
	float MinY = -1050.f;
	float MaxY = 1050.f;
	float MinX = -410.f;
	float MaxX = 410.f;
	float SpawnZ = 0;

	float MinYKill = -940.f;
	float MaxYKill = 940.f;
	float MinXKill = -295.f;
	float MaxXKill = 295.f;
	float KillZ = 0;
	float StepDelayMalus = 5.f;
	float StepDelayAddScore = 10.f;
	float StepDelaySpeed = 15.f;
	float BufferTimeMalus = 0;
	float BufferTimeAddScore = 0;
	float BufferTimeSpeed = 0;
	
	UPROPERTY(BlueprintAssignable, Category = "EventDispatchers")
	FGameOver GameOver;
	UPROPERTY(BlueprintAssignable, Category = "EventDispatchers")
	FWin YouWin;


	
	
};

