// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Components/InputComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Food.h"
#include <iostream>



// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;


}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	
	FInputModeGameOnly InputMode;
	UGameplayStatics::GetPlayerController(GetWorld(), 0)->SetInputMode(InputMode);
	//CreateSnakeActor();
	//AddRandomFood();
	
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	BufferTimeMalus += DeltaTime;
	if (BufferTimeMalus > StepDelayMalus)
	{
		InstantKill();
		BufferTimeMalus = 0;

	}

	BufferTimeAddScore += DeltaTime;
	if (BufferTimeAddScore > StepDelayAddScore)
	{
		SpawnScoreBonus();
		BufferTimeAddScore = 0;

	}

	BufferTimeSpeed += DeltaTime;
	if (BufferTimeSpeed > StepDelaySpeed)
	{
		SpawnSpeedBonus();
		BufferTimeSpeed = 0;

	}
}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
	SnakeActor->SetOwner(this);

}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor) &&!SnakeActor->Moving)
	{
		if (value > 0 &&SnakeActor->LastMoveDirection!=EMovementDirection::DOWN)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::UP;
			SnakeActor->Moving = true;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::UP)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
			SnakeActor->Moving = true;
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor) &&!SnakeActor->Moving)
	{
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
			SnakeActor->Moving = true;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGHT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
			SnakeActor->Moving = true;
		}
	}
}

void APlayerPawnBase::AddRandomFood()
{
	FRotator StartPointRotation = FRotator(0, 0, 0);
	float SpawnX = FMath::FRandRange(MinX, MaxX);
	float SpawnY = FMath::FRandRange(MinY, MaxY);
	FVector StartPoint = FVector(SpawnX, SpawnY, SpawnZ);

	if (IsValid(SnakeActor))
	{
		FoodActor = GetWorld()->SpawnActor<AFood>(FoodActorClass,StartPoint, StartPointRotation);
		FoodActor->BonusActivated.AddDynamic(this, &APlayerPawnBase::AddRandomFood);
	}

}

void APlayerPawnBase::InstantKill()
{
	FRotator StartPointRotation = FRotator(0, 0, 0);
	float SpawnX = FMath::FRandRange(MinXKill, MaxXKill);
	float SpawnY = FMath::FRandRange(MinYKill, MaxYKill);
	FVector StartPoint = FVector(SpawnX, SpawnY, SpawnZ);
	if (IsValid(SnakeActor))
	{
		DeathActor = GetWorld()->SpawnActor<ADeathMalus>(DeathActorClass, StartPoint, StartPointRotation);

	}
}

void APlayerPawnBase::SpawnSpeedBonus()
{
	FRotator StartPointRotation = FRotator(0, 0, 0);
	float SpawnX = FMath::FRandRange(MinX, MaxX);
	float SpawnY = FMath::FRandRange(MinY, MaxY);
	FVector StartPoint = FVector(SpawnX, SpawnY, SpawnZ);

	if (IsValid(SnakeActor))
	{
		BonusActor = GetWorld()->SpawnActor<ABonus>(BonusActorClass, StartPoint, StartPointRotation);

	}
}

void APlayerPawnBase::SpawnScoreBonus()
{
	FRotator StartPointRotation = FRotator(0, 0, 0);
	float SpawnX = FMath::FRandRange(MinXKill, MaxXKill);
	float SpawnY = FMath::FRandRange(MinYKill, MaxYKill);
	FVector StartPoint = FVector(SpawnX, SpawnY, SpawnZ);
	if (IsValid(SnakeActor))
	{
		ScoreBonusActor = GetWorld()->SpawnActor<AScoreBonus>(ScoreBonusActorClass, StartPoint, StartPointRotation);
		
	}
}

void APlayerPawnBase::ScoreUP()
{
	Score++;
	if (Score >= WinScore)
	{
		YouWin.Broadcast();
	}
}

void APlayerPawnBase::BonusScoreUP()
{
	Score += 50;
	if (Score >= WinScore)
	{
		YouWin.Broadcast();
	}
	
}

void APlayerPawnBase::LifeDecrease()
{
	Life--;
	CreateSnakeActor();
	if (Life <= 0)
	{
		GameOver.Broadcast();
	}
}















